package com.example.exceptiontry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExceptiontryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptiontryApplication.class, args);
	}

}
