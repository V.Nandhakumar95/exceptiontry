package com.example.exceptiontry.sample;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

//if the studentid comes 1 means throw this message
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> handleException(IllegalArgumentException e){
		return new ResponseEntity<Object> ("Illegal argument exception in controller",HttpStatus.BAD_REQUEST);
	}
}
