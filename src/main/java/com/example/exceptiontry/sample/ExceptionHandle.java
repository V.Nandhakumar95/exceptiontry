package com.example.exceptiontry.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//coderulagam exception handling video followed
@RestController
public class ExceptionHandle {
	
	@GetMapping(value="/students/{studentid}")
//	public ResponseEntity<StudentDto> getStudent(@RequestParam ("studentid") Integer studentid){
	public ResponseEntity<StudentDto> getStudent(@PathVariable ("studentid") Integer studentid){	
		
//  for path variable in postman given url http://localhost:8095/stu/students/2		
//  for Request param in postman given url http://localhost:8095/stu/students/studentid?studentid=1		
		StudentDto studentDto = new StudentDto();
		studentDto.setId(1);
		studentDto.setName("vnk");
		studentDto.setAge(12);
		
		if(studentid==1) {
			throw new IllegalArgumentException();
		}
		
		if(studentid==2) {
			throw new IllegalStateException();
		}
		
		return new ResponseEntity<StudentDto>(studentDto,HttpStatus.OK);
	}

//if the studentid comes 2 means throw this message	
// controller specific logic means we'll handle in controller also like below
// 1st level of priority is controller level	
	@ExceptionHandler(value= {IllegalStateException.class})
	public ResponseEntity<String> handleException(IllegalStateException e){
		return new ResponseEntity<String>("Illegal state exception in controller",HttpStatus.BAD_REQUEST);
	}
	
	
}
